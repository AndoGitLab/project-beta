from django.db import models
#from inventory.api.inventory_rest.models import Automobile

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name


class Appointment(models.Model):
    appointment_vin = models.CharField(max_length=17, unique=True)
    owner = models.CharField(max_length=200)
    date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return self.owner
