from django.contrib import admin
from django.urls import path
from .views import api_list_technician, api_show_technician, api_list_appointment, api_show_appointment

urlpatterns = [
    path("technicians/", api_list_technician, name="api_create_technician"),
    path("technicians/<int:id>/", api_show_technician, name="api_show_technician"),
    path("appointments/", api_list_appointment, name="api_create_appointment"),
    path("appointments/<int:id>/", api_show_appointment, name="api_show_appointment"),
]
