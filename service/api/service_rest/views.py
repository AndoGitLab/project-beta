from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .encoders import (
    TechnicianListEncoder,
    AutomobileEncoder,
    AppointmentEncoder,
)
from .models import AutomobileVO, Technician, Appointment

@require_http_methods(["GET", "POST"])
def api_list_technician (request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_technician (request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id = id)
            return JsonResponse(
                {"technician": technician},
                encoder= TechnicianListEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist."},
                status=400
            )

    else:
        #Delelte Technician
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_appointment (request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder= AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment (request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id = id)
            return JsonResponse(
                {"appointment": appointment},
                encoder= AppointmentEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist."},
                status=400
            )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist."},
                status=400
            )

    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
