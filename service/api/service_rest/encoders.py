from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
import json

class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "appointment_vin",
        "owner",
        "technician",
        "reason",
        "finished",
    ]
    encoders = {"technician": TechnicianListEncoder()}

    def get_extra_data(self, o):
        date = json.dumps(o.date, default=str)
        time = json.dumps(o.time, default=str)
        date = json.loads(date)
        time = json.loads(time)
        try:
            AutomobileVO.objects.get(vin=o.appointment_vin)
            return{"vip": True, "date" : date, "time" : time}
        except:
            return {"vip": False, "date" : date, "time" : time}
