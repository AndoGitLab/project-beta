import React, { Component } from 'react'

class AppointmentList extends React.Component{
    state = {
        "appointments": []
    }

    async componentDidMount() {
        await this.getAppointments()

    }

    async getAppointments() {
        const response  = await fetch('http://localhost:8080/api/appointments')

        if (response.ok) {
            let data = await response.json()
            console.log(data)
            let filteredData = await data.appointments.filter(appointment => appointment.finished == false)
            this.setState({"appointments": filteredData})
        }
    }

    async handleDeleteClick(id) {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application.json'
            }
        }
        await fetch(url, fetchConfig)
        this.getAppointments()
    }

    async handleFinishClick(id) {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({
                "finished": true
            }),
            headers: {
                'Content-Type': 'application.json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            this.getAppointments()
        }
    }

    render () {
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIP Status</th>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{ String(appointment.vip) }</td>
                                <td>{ appointment.appointment_vin }</td>
                                <td>{ appointment.owner }</td>
                                <td>{ appointment.date }</td>
                                <td>{ appointment.time }</td>
                                <td>{ appointment.technician.name }</td>
                                <td>{ appointment.reason }</td>
                                <td><button onClick={() => this.handleDeleteClick(appointment.id)} className="btn btn-danger m2">Cancel</button></td>
                                <td><button onClick={() => this.handleFinishClick(appointment.id)} className="btn btn-success m2">Finish</button></td>
                            </tr>
                        );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default AppointmentList;
