import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import AutomobileForm from './AutomobileForm';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelFrom from './VehicleModelForm';
import SalesList from './SalesList';
import ManufacturerList from './ManufacturerList';
import VehicleList from './VehicleList';
import AutomobileList from './AutomobileList';
import SalesForm from './SalesForm';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<AutomobileList automobile={props.manufacturer} />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />

          <Route path="/manufacturers" element={<ManufacturerList manufacturer={props.manufacturer} />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />

          <Route path="/vehicles" element={<VehicleList vehicle={props.manufacturer} />} />
          <Route path="vehicles/new" element={<VehicleModelFrom />} />

          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
            <Route path="" element={<AppointmentList />} />
          </Route>
          <Route path="history">
            <Route path="" element={<ServiceHistory />} />
          </Route>

          <Route path="/sales" element={<SalesList sales={props.sales} />} />
          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/salesperson/new" element={<SalesPersonForm/>}/>
          <Route path="/customer/new" element={<CustomerForm/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
