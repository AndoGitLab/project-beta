import {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom';


function ManufacturerList() {
    const [manufacturers, setManufacturer]  = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/manufacturers/')
        const data = await resp.json()

        setManufacturer(data.manufacturers)
    }
    useEffect(()=> {
        getData();
    }, [])

    return (
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Manufacturers</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map(manufacturer => {
                return (
                  <tr key={manufacturer.id}>
                    <td>{ manufacturer.name }</td>
                    <td><button onClick={async ()=>{
                      const resp = await fetch(`http://localhost:8100/api/manufacturers/${manufacturer.id}/`, { method:"DELETE"})
                      const data = await resp.json()
                      getData()
                      }}>Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div>
            <NavLink className="nav-link" aria-current="page" to="new"><button>Add a Manufacturer</button></NavLink>

          </div>
        </div>

        );
      }

  export default ManufacturerList
