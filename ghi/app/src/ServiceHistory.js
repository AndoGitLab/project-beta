import React from 'react';

class ServiceHistory extends React.Component {
    state = {
        appointments: [],
        filterVIN: "",
    }


    async getServiceHistory() {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if(response.ok) {
            const data = await response.json();
            console.log(data)
            const appointments = data.appointments;
            this.setState({appointments: appointments});
        }
    }


    handleInputChange = (e) => {
        this.setState({
            filterVIN: e.target.value
        });
    }

    async componentDidMount() {
        await this.getServiceHistory();
    }



    render() {
        return (
            <div>
            <h1 className="mt-3">Service History</h1>
            <div className="input-group mt-3 mb-3">
                <input
                    onChange={this.handleInputChange}
                    className="form-control rounded"
                    placeholder="Search VIN"
                />
            </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments
                .filter((appointment) =>
                appointment.appointment_vin.includes(this.state.filterVIN))
                .map((appointment) =>
                    <tr key={appointment.id}>
                        <td>{ appointment.appointment_vin }</td>
                        <td>{ appointment.owner }</td>
                        <td>{ appointment.date }</td>
                        <td>{ appointment.time }</td>
                        <td>{ appointment.technician.name }</td>
                        <td>{ appointment.reason }</td>
                    </tr>
                )}
            </tbody>
        </table>
        </div>
        );
    }
}

export default ServiceHistory;
