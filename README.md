# CarCar

Team:

* Anderson Ngo - Sales Microservice
* Adrian Chen - Services Microservice


## Design

We designed a fullstack application that is similar to a car website enabling the user to manage the sales of cars. There is a nav link to have access to the sales and services parts of the project. We also used polling to connect microservices together to get data from their databases. We restfulized our api frameworks to implement CRUD functionalities and connected our backend to the front using react.


## Service microservice

Adrian-

There are three models in the service microservice -- AutomobileVO, Technician and Appointmnet. The first model, the AutomobileVO model polls information from the Automobile model within the Inventory microservice via a poller. There are two properties on the automobile VO, vin and import_href which correspond to the vin and href of the Automobile model. Both vin and import_href are unique.

The second model, the Technician model has two properties, name and employee_number.

The third model, the Appointment model has 8 properties -- appointment_vin, owner, date, time, technician, reason, vip and finished. Appointment_vin field has unique values because each car can have only one vin. Technician is a foreign key on the model. Vip and finished are set to false by default because there is validation process in encoders for vip and appointments are unfinished by default.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.


1- AutomobileVO, was a VO because it derived from inventory microservice, and I only gave it a vin property because we it was all we needed to identify the car's uniquenes and which was why it was unique.
2- SalesPerson, had 2 properties sales_person_name, and employee_number. I made employee_number unqiue to identify each employee.
3- Customer, had 3 properties, customer_name, address, and phone_number. I made the address and phone numbers unique assuming people can have the same names but lived in different address and had differnt #'s.
4- Sales, had 4 properties vin, sales_person, customer, and sales_price. Vin, sales_person, and customer were all foreign keys from the previous models, and sales price was on its own. We also defined a method called get_api_url, and used the reverse method to return the URL for api_show_sale view passing in the kwarg of pk=self.id and telling the kwarg to tell django to look for the pk value in the URL. We're using the id value to get the sale object and returning the URL.


The sales microservice intergrates with the inventory microservice by getting the data through polling, specifically for the AutomobileVO so we can get the VIN number because in order to create a new sale or list the sales, since we would need to have that information.
