from django.db import models
from django.urls import reverse
# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    sales_person_name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=17, unique=True)


class Customer(models.Model):
    customer_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, unique=True)
    phone_number = models.CharField(max_length=10, unique=True, default=None)




class Sales(models.Model):
    vin = models.ForeignKey(
        AutomobileVO, related_name="automobile", on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        SalesPerson, related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="customers", on_delete=models.CASCADE
    )
    sales_price = models.PositiveIntegerField()

    def get_api_url(self):
            return reverse("api_show_sale", kwargs={"pk": self.id})
