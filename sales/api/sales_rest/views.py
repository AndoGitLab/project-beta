from django.shortcuts import render
from .models import AutomobileVO, Sales, SalesPerson, Customer
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Sales
from .encoders import SalesPersonEncoder, CustomerEncoder, SalesListEncoder
# Create your views here.



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse({
            "sales": sales},
            encoder=SalesListEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.get(id=content["customer"])
        content['customer'] = customer
        sales_person = SalesPerson.objects.get(employee_number=content['sales_person'])
        content['sales_person'] = sales_person
        vin = AutomobileVO.objects.get(vin=content['vin'])
        content['vin'] = vin
    sale = Sales.objects.create(**content)
    return JsonResponse(
        sale,
        encoder=SalesListEncoder,
        safe=False,
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        sale = Sales.objects.get(pk=pk)
        return JsonResponse(
            sale,
            encoder=SalesListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _= Sales.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Sales.objects.filter(pk=pk).update(**content)
            sale = Sales.objects.get(pk=pk)
            return JsonResponse(
                sale,
                encoder=SalesListEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales"},
                status=400
            )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({
            "customers": customers},
            encoder=CustomerEncoder)
    else:
        try:
            content = json.loads(request.body)
            print(content)
            customer = Customer.objects.create(**content)
            print(customer)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"},
                status=400
            )



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request,id=None):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _= Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Customer.objects.filter(id=id).update(**content)
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales"},
                status=400
            )



@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": salespeople},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = SalesPerson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Sales person does not exist"},
                status=400
            )
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_salesperson(request,id=None):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _= SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            SalesPerson.objects.filter(id=id).update(**content)
            salesperson = SalesPerson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales"},
                status=400
            )
